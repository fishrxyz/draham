class ConfigNotFoundError(BaseException):
    pass


class NoAPIKeyError(BaseException):
    pass
