from draham.draham import Draham
import pytest


@pytest.fixture(scope='function')
def draham():
    api_key = "api_key"
    symbols = "BTC,ETH"
    currency = "USD"

    d = Draham(api_key, symbols, currency)
    yield d


@pytest.fixture(scope='module')
def quotes():
    quotes_list = {
        'BCH': {
            'id': 1831,
            'name': 'Bitcoin Cash',
            'symbol': 'BCH',
            'slug': 'bitcoin-cash',
            'num_market_pairs': 587,
            'date_added': '2017-07-23T00:00:00.000Z',
            'tags': ['mineable', 'marketplace', 'enterprise-solutions', 'binance-chain'],
            'max_supply': 21000000,
            'circulating_supply': 18668887.5,
            'total_supply': 18668887.5,
            'is_active': 1,
            'platform': None,
            'cmc_rank': 10,
            'is_fiat': 0,
            'last_updated': '2021-03-02T16:09:09.000Z',
            'quote': {
                'USD': {
                    'price': 535.9095533022614,
                    'volume_24h': 4393865157.491896,
                    'percent_change_1h': 0.2034256,
                    'percent_change_24h': 8.80937666,
                    'percent_change_7d': 5.10988551,
                    'percent_change_30d': 33.36379708,
                    'market_cap': 10004835160.775171,
                    'last_updated': '2021-03-02T16:09:09.000Z'
                }
            }
        },
        'ETH': {
            'id': 1839,
            'name': 'Binance Coin',
            'symbol': 'BNB',
            'slug': 'binance-coin',
            'num_market_pairs': 478,
            'date_added': '2017-07-25T00:00:00.000Z',
            'tags': ['marketplace', 'payments'],
            'max_supply': 170532785,
            'circulating_supply': 154532785,
            'total_supply': 170532785,
            'is_active': 1,
            'platform': None,
            'cmc_rank': 4,
            'is_fiat': 0,
            'last_updated': '2021-03-02T16:09:09.000Z',
            'quote': {
                'USD': {
                    'price': 246.6954600658253,
                    'volume_24h': 5000869595.581762,
                    'percent_change_1h': -1.647281,
                    'percent_change_24h': -1.3125952,
                    'percent_change_7d': 8.91236433,
                    'percent_change_30d': 460.2653425,
                    'market_cap': 38122536490.82827,
                    'last_updated': '2021-03-02T16:09:09.000Z'
                }
            }
        }
    }

    yield quotes_list


