from draham.utils import round_percent

def test_format_supply(draham):
    supply = draham._format_supply(123456789.123456, 'XRP')
    assert supply == "123,456,789 XRP"


def test_format_number(draham):
    result = draham._format_number(45841.9868, '$')
    assert result == "$45,841"


def test_format_number_no_currency(draham):
    result = draham._format_number(45841.9868, currency=False)
    assert result == "45,841"


def test_get_raw_quotes(draham, mocker):

    endpoint = '/cryptocurrency/quotes/latest'
    params = {
        'symbol': draham.symbols,
        'convert': draham.currency
    }
    mocked_call_api = mocker.patch.object(draham, '_call_api', return_value={})
    raw_quotes = draham._get_raw_quotes()

    mocked_call_api.assert_called_with(endpoint, params=params)


def test_parse_quotes(draham, mocker, quotes):

    mocked_raw_quotes = mocker.patch.object(
        draham,
        '_get_raw_quotes',
        return_value=quotes
    )
    clean_quotes = draham._parse_quotes()

    fields = [
        'rank', 'symbol', 'price',
        'price_1h', 'price_24h',
        'price_7d', 'market_cap',
        'volume_24h', 'circulating_supply'
    ]

    for quote in clean_quotes:
        assert list(quote.keys()) == fields
