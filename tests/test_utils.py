import os
import pytest
from configparser import ConfigParser
from termcolor import colored
from draham.constants import QUOTES_TABLE_HEADER
from draham.exceptions import NoAPIKeyError, ConfigNotFoundError
from draham.utils import *


def test_check_config_not_found():
    parser = ConfigParser()
    conf = os.path.join(os.getcwd(), 'tests/files/notfound')

    with pytest.raises(ConfigNotFoundError):
        check_config(parser, conf)


def test_check_config_no_api_key():
    conf = os.path.join(os.getcwd(), 'tests/files/config_no_auth.ini')
    parser = ConfigParser()

    with pytest.raises(NoAPIKeyError):
        check_config(parser, conf)


def test_print_error():

    with pytest.raises(SystemExit) as exc:
        error = "THIS IS AN ERROR !!!!"
        print_error(error)

    assert exc.value.args[0] == colored(f'[!] ERROR - {error}', 'red')


def test_parse_error():
    error = {'status': { 'error_message': 'hehe' }}
    parsed_error = parse_error(error, 400)
    assert parsed_error == f'(400) {error["status"]["error_message"]}'


def test_round_percent_positive():
    percent = round_percent(2.19287)
    assert percent == colored("+2.19%", 'green')


def test_round_percent_negative():
    percent = round_percent(-4.9823928)
    assert percent == colored("-4.98%", 'red')


def test_get_basic_table_columns():
    fields = get_basic_table_columns()
    expected_fields = [
        'rank', 'symbol', 'price',
        'price_24h', 'price_7d', 'market_cap'
    ]
    assert fields == [QUOTES_TABLE_HEADER[field] for field in expected_fields]
